package net.bartipan.iomonkey2;

/**
 * Created by jbartipan on 15.08.16.
 */
public class BaseException extends RuntimeException {
    protected BaseException(String message) {
        super(message);
    }
}
