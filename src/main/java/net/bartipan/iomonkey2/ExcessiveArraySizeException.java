package net.bartipan.iomonkey2;

public class ExcessiveArraySizeException extends BaseException {
    private int _itemCountLimit;
    private int _actualItemCount;

    public ExcessiveArraySizeException(int limit, int actualCount) {
        super(String.format("Array size (%d) exceeds allowed limit (%d) while reading binary stream", actualCount, limit));
        _itemCountLimit = limit;
        _actualItemCount = actualCount;
    }

    public int getItemCountLimit() {
        return _itemCountLimit;
    }

    public int getActualItemCount() {
        return _actualItemCount;
    }
}
