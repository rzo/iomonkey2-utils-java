package net.bartipan.iomonkey2;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;

/**
 * Created by jbartipan on 27.01.16.
 */
public class LoadSaveHelper {
    public static int computeLeb128ByteSize(int num) {
        int ret = 1;
        while ((num >>= 7) > 0) ret++;
        return ret;
    }

    public static int utf8Length(CharSequence sequence) {
        int count = 0;
        for (int i = 0, len = sequence.length(); i < len; i++) {
            char ch = sequence.charAt(i);
            if (ch <= 0x7F) {
                count++;
            } else if (ch <= 0x7FF) {
                count += 2;
            } else if (Character.isHighSurrogate(ch)) {
                count += 4;
                ++i;
            } else {
                count += 3;
            }
        }
        return count;
    }

    public static int computeStringStorage(String s) {
        int n = utf8Length(s);
        return n + computeLeb128ByteSize(n);
    }


    public static void writeNullIndices(DataOutput dout,  Object[] vals) throws IOException {
        short nullCount = 0;
        for (int i = 0; i < vals.length; i++) if (vals[i] == null) nullCount++;
        dout.writeShort(nullCount);
        for (int i = 0; i < vals.length; i++) if (vals[i] == null) dout.writeInt(i);
    }

    public static void readNullIndices(DataInput din, List<Integer> outIndices) throws IOException {
        outIndices.clear();
        short nullCount = din.readShort();
        while (nullCount-- > 0) outIndices.add(din.readInt());
    }
}
