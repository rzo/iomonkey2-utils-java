package net.bartipan.iomonkey2;

/**
 * Created by jbartipan on 10.03.15.
 */
public class FixedValueException extends BaseException {
    private Object _expectedValue;
    private  Object _gotValue;
    private Class _ioClass;

    public FixedValueException(Object expected, Object got) {
        super("Expected '" + expected.toString() + "' but got '" + got.toString() + "'");
        _expectedValue = expected;
        _gotValue = got;
        _ioClass = null;
    }

    public FixedValueException(Object expected, Object got, Class ioClass) {
        super("Expected '" + expected.toString() + "' but got '" + got.toString()
                + "' while loading instance of " + ioClass.getName());
        _expectedValue = expected;
        _gotValue = got;
        _ioClass = ioClass;
    }

    public Object getExpectedValue() {
        return _expectedValue;
    }

    public Object getGotValue() {
        return _gotValue;
    }

    public Class getIoClass() {
        return _ioClass;
    }
}
