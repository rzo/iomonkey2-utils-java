package net.bartipan.iomonkey2.utils;

import java.io.*;

/**
 * Created by jbartipan on 05.12.14.
 */
public class LittleEndianDataOutputStream extends FilterOutputStream implements DataOutput {
    public static int reorderWord(int v) {
        int b0 = v & 0xff;
        int b1 = (v >> 8) & 0xff;
        return (b0 << 8) | b1;
    }

    public static int reorderDWord(int v) {
        int b0 = v & 0xff;
        int b1 = (v >> 8) & 0xff;
        int b2 = (v >> 16) & 0xff;
        int b3 = (v >> 24) & 0xff;
        return (b0 << 24) | (b1 << 16) | (b2 << 8) | b3;
    }

    public static long reorderQWord(long v) {
        return (((long)reorderDWord((int)(v & 0x00000000ffffffffL)) & 0x00000000ffffffffL) << 32L)
                | (reorderDWord((int)((v >> 32) & 0x00000000ffffffffL)) & 0x00000000ffffffffL);
    }

    public LittleEndianDataOutputStream(OutputStream out) {
        super(new DataOutputStream(out));
    }

    @Override
    public void write(byte[] b) throws IOException {
        ((DataOutputStream)out).write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        ((DataOutputStream)out).write(b, off, len);
    }

    @Override
    public void writeBoolean(boolean v) throws IOException {
        ((DataOutputStream)out).writeBoolean(v);
    }

    @Override
    public void writeByte(int v) throws IOException {
        ((DataOutputStream)out).writeByte(v);
    }

    @Override
    public void writeShort(int v) throws IOException {
        ((DataOutputStream)out).writeShort(reorderWord(v));
    }

    @Override
    public void writeChar(int v) throws IOException {
        ((DataOutputStream)out).writeChar(reorderWord(v));
    }

    @Override
    public void writeInt(int v) throws IOException {
        ((DataOutputStream)out).writeInt(reorderDWord(v));
    }

    @Override
    public void writeLong(long v) throws IOException {
        ((DataOutputStream)out).writeLong(reorderQWord(v));
    }

    @Override
    public void writeFloat(float v) throws IOException {
        ((DataOutputStream)out).writeInt(reorderDWord(Float.floatToRawIntBits(v)));
        //((DataOutputStream)out).writeFloat(Float.intBitsToFloat(reorderDWord(Float.floatToRawIntBits(v))));
    }

    @Override
    public void writeDouble(double v) throws IOException {
        ((DataOutputStream)out).writeLong(reorderQWord(Double.doubleToRawLongBits(v)));
        //((DataOutputStream)out).writeDouble(Double.longBitsToDouble(reorderQWord(Double.doubleToRawLongBits(v))));
    }

    @Override
    public void writeBytes(String s) throws IOException {
        ((DataOutputStream)out).writeBytes(s);
    }

    @Override
    public void writeChars(String s) throws IOException {
        ((DataOutputStream)out).writeChars(s);
    }

    @Override
    public void writeUTF(String s) throws IOException {
        byte[] bytes = s.getBytes("UTF-8");
        writeLeb128(bytes.length);
        write(bytes);
    }

    private void writeLeb128(long val) throws IOException {
        do {
            byte b = (byte)(val & 0x7f);
            val >>= 7;
            if (val > 0)
                b |= 0x80;
            out.write(b);
        } while (val != 0);
    }
}
