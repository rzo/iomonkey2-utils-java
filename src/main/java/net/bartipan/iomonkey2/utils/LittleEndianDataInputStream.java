package net.bartipan.iomonkey2.utils;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by jbartipan on 05.12.14.
 */
public final class LittleEndianDataInputStream extends FilterInputStream implements DataInput {

    private byte[] _buf = new byte[80];
    public LittleEndianDataInputStream(InputStream in) {
        super(in);
    }

    @Override
    public void readFully(byte[] b) throws IOException {
        readFully(b, 0, b.length);
    }

    @Override
    public void readFully(byte[] b, int off, int len) throws IOException {
        if (len < 0)
            throw new IndexOutOfBoundsException();
        int n = 0;
        while (n < len) {
            int count = in.read(b, off + n, len - n);
            if (count < 0)
                throw new EOFException();
            n += count;
        }
    }

    @Override
    public int skipBytes(int n) throws IOException {
        return (int)in.skip(n);
    }

    @Override
    public boolean readBoolean() throws IOException {
        return readByteSafe() != 0;
    }

    @Override
    public byte readByte() throws IOException {
        return (byte)readUnsignedByte();
    }

    @Override
    public int readUnsignedByte() throws IOException {
        int b = in.read();
        if (b < 0)
            throw new EOFException();
        return b;
    }

    @Override
    public short readShort() throws IOException {
        return (short)readUnsignedShort();
    }

    @Override
    public int readUnsignedShort() throws IOException {
        int b1 = readUnsignedByte();
        int b2 = readUnsignedByte();
        return (b2 << 8) | (b1);
    }

    @Override
    public char readChar() throws IOException {
        return (char)readUnsignedShort();
    }

    @Override
    public int readInt() throws IOException {
        readFully(_buf, 0, 4);
        return ByteBuffer.wrap(_buf, 0, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
    }

    @Override
    public long readLong() throws IOException {
        readFully(_buf, 0, 8);
        return ByteBuffer.wrap(_buf, 0, 8).order(ByteOrder.LITTLE_ENDIAN).getLong();
    }

    @Override
    public float readFloat() throws IOException {
        readFully(_buf, 0, 4);
        return ByteBuffer.wrap(_buf, 0, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
    }

    @Override
    public double readDouble() throws IOException {
        readFully(_buf, 0, 8);
        return ByteBuffer.wrap(_buf, 0 ,8).order(ByteOrder.LITTLE_ENDIAN).getDouble();
    }

    @Override
    public String readLine() throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String readUTF() throws IOException {
        int len = (int) readLeb128();
        byte[] buf = len <= _buf.length ? _buf : new byte[len];
        readFully(buf, 0, len);

        return new String(buf, 0, len, "UTF-8");
    }

    private byte readByteSafe() throws IOException, EOFException {
        int b = in.read();
        if (b == -1)
            throw new EOFException();
        return (byte)b;
    }

    private long readLeb128() throws IOException {
        long ret = 0;
        int b;
        int off = 0;
        do {
            b = readUnsignedByte();
            ret |= (b & 0x7f) << off;
            off += 7;
        } while ((b & 0x80) != 0);

        return ret;
    }
}
