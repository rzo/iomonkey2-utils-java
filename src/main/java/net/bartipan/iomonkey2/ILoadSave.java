package net.bartipan.iomonkey2;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public interface ILoadSave
{
    public void load(DataInput dis) throws IOException;
    public void save(DataOutput dos) throws IOException;
}
